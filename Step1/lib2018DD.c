#include "lib2018DD.h"

void AProposDD(char *Version,char *Nom1,char* Nom2)
{
    printf("Version : %s \n",Version ) ;
    printf("Nom1 : %s \n",Nom1 ) ;
    printf("Nom2 : %s \n",Nom2 ) ;
}

int RechercheDD(char* NomFichier,int Reference ,struct ArticleDD
*UnRecord){

    FILE* f = fopen(NomFichier,"r");

    if(!f)
        return -1; //fail ouverture fichier

    while(!feof(f)) {
        fread(UnRecord, sizeof(*UnRecord), 1, f);
        if(UnRecord->Reference == Reference){
            fclose(f);
            return Reference;
        }

    }

    fclose(f);
    return 0;

}


