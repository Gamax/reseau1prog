#ifndef LIB2018DD_H
#define LIB2018DD_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "data.h"


void AProposDD(char *Version,char *Nom1,char* Nom2) ;

int RechercheDD(char* NomFichier,int Reference ,struct ArticleDD
*UnRecord) ;

#endif LIB2018DD_H
