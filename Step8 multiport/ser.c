/*--------------------------------------
  Herman Vanstapel
  
  ex02\ser.c 
  
  Un serveur recevant une structure 
----------------------------------------*/

#include <stdio.h>
#include <string.h>
#include "../udplib/udplib.h"
#include "structure.h"
#include "lib2018DD.h"
#include "data.h"
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>

pthread_mutex_t mutex1 = PTHREAD_MUTEX_INITIALIZER; // Toujours déclaré public

struct STDD {
/* XX vos initiales  ̈*/
    int DescPublic ;
    struct sockaddr_in psorPublic ; /* r = remote */
    struct RequeteDD UneRequeteR ;
} ;

void *ThreadRecherche ( void* Param ) {
    struct STDD *pST;
    struct RequeteDD UneRequete,UneRequete2;
    struct ArticleDD UnRecord;
    int DescPublic;
    struct sockaddr_in psorPublic;
    struct RequeteDD UneRequeteR;
    int res, rc;
    fprintf(stderr, "YOLO1\n");
    fprintf(stderr, "Demarrage du Thread & attente section critique \n");
    pST = (struct STDD *) Param;
    DescPublic = pST->DescPublic;
    psorPublic = pST->psorPublic;
    UneRequete = pST->UneRequeteR;

    fprintf(stderr, "YOLO2 %d\n",DescPublic);

    res = RechercheDD("ArticlesDD", UneRequete.Reference, &UnRecord);

    strncpy(UneRequete2.Description, UnRecord.Description, sizeof(UneRequete2.Description));

    UneRequete2.Quantite = UnRecord.Quantite;
    UneRequete2.Numero = UneRequete.Numero;
    UneRequete2.Reference = UneRequete.Reference;
    UneRequete2.DimensionEcran = UnRecord.DimensionEcran;

    fprintf(stderr, "YOLO3\n");

    if (res>0)
        UneRequete2.Type = OK;
    else
        UneRequete2.Type = Fail;

    rc = SendDatagram(DescPublic, &UneRequete2, sizeof(struct RequeteDD), &psorPublic);

    if (rc == -1)
        perror("SendDatagram:");
    else
        fprintf(stderr, "bytes:%d\n", rc);

    pthread_exit(NULL);

}

void *ThreadFacturation ( void* Param ) {
    struct STDD *pST;
    struct RequeteDD UneRequete,UneRequete2;
    struct ArticleDD UnRecord;
    int DescPublic;
    struct sockaddr_in psorPublic;
    struct RequeteDD UneRequeteR;
    int res, rc;
    fprintf(stderr, "YOLO1\n");
    fprintf(stderr, "Demarrage du Thread & attente section critique \n");
    pST = (struct STDD *) Param;
    DescPublic = pST->DescPublic;
    psorPublic = pST->psorPublic;
    UneRequete = pST->UneRequeteR;

    fprintf(stderr, "YOLO2 %d\n",DescPublic);

    pthread_mutex_lock( &mutex1 );
    fprintf(stderr,"Entrée Section critique\n") ;
    sleep(5) ;

    res = ReservationDD("ArticlesDD",UneRequete.Reference,UneRequete.Quantite);

    pthread_mutex_unlock( &mutex1 );
    fprintf(stderr,"Sortie Section critique\n") ;



    if (res>0) {
        UneRequete2.Type = OK;
        UneRequete2.NumeroFacture = FacturationDD("FactureDD",UneRequete.NomClient,UneRequete.Date,UneRequete.Quantite,UneRequete.Reference);


    }
    else
        UneRequete2.Type = Fail;

    SendDatagram(DescPublic,&UneRequete2, sizeof(struct RequeteDD),&psorPublic);

    pthread_exit(NULL);

}

void die(char *s)
{
    perror(s);
    exit(1);
}

char ReadChar()
{
    char Tampon[80] ;
    fgets(Tampon,sizeof Tampon,stdin ) ;
    return Tampon[0] ;
}

void Traitementrequete(int Desc){

    int rc ;
    struct sockaddr_in sor ; /* r = remote */

    int res,res2;

    int tm ;
    struct RequeteDD UneRequete, UneRequete2;

    struct ArticleDD UnRecord;

    tm = sizeof(struct RequeteDD);
    rc = ReceiveDatagram(Desc, &UneRequete, tm, &sor);
    if (rc == -1)
        die("ReceiveDatagram");
    else
        fprintf(stderr, "bytes recus:%d:%s\n", rc, UneRequete.Description);

    printf("Received packet from %s:%d\n", inet_ntoa(sor.sin_addr), ntohs(sor.sin_port));

    struct STDD *pST ;
    pthread_attr_t attr;


    switch (UneRequete.Type) {
        case Question:
            fprintf(stderr, "A rechercher %d \n", UneRequete.Reference);

            fprintf(stderr, "YOLO0\n");

            pST = malloc( sizeof( struct STDD)) ;
            pST->DescPublic = Desc ;
            pST->psorPublic = sor ;
            pST->UneRequeteR = UneRequete ;

            pthread_t thread1_id;

            fprintf(stderr, "YOLO1\n");

            rc=pthread_attr_init(&attr);
            rc=pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
            rc=pthread_create (&thread1_id, NULL,  (void * (*) (void *))ThreadRecherche,pST );

            //fprintf(stderr,"res :%d Reference:%s Quantité %d\n",res,UnRecord.Description,UnRecord.Quantite ) ;

/* reponse avec psor qui contient toujours l'adresse du dernier client */


            break;
        case Achat:
            fprintf(stderr, "A acheter %d \n", UneRequete.Reference);

            pST = malloc( sizeof( struct STDD)) ;
            pST->DescPublic = Desc ;
            pST->psorPublic = sor ;
            pST->UneRequeteR = UneRequete ;

            pthread_t thread2_id;

            fprintf(stderr, "YOLO1\n");

            rc=pthread_attr_init(&attr);
            rc=pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
            rc=pthread_create (&thread2_id, NULL,  (void * (*) (void *))ThreadFacturation,pST );



            break;

        default:
            break;
    }
}

int main(int argc,char *argv[])
{
    int rc ;
    int Desc1,Desc2,Desc3 ;
    struct sockaddr_in sthis1 ; /* this ce programme */
    struct sockaddr_in sthis2 ; /* this ce programme */
    struct sockaddr_in sthis3 ; /* this ce programme */

    char car;

    fd_set readfs ;
    struct timeval tv ;

    struct sockaddr_in sos ; /* s = serveur */
    struct sockaddr_in sor ; /* r = remote */

    u_long  IpSocket ;
    u_short PortSocket ;

    memset(&sthis1,0,sizeof(struct sockaddr_in)) ;
    memset(&sthis2,0,sizeof(struct sockaddr_in)) ;
    memset(&sthis3,0,sizeof(struct sockaddr_in)) ;
    memset(&sos,0,sizeof(struct sockaddr_in)) ;
    memset(&sor,0,sizeof(struct sockaddr_in)) ;

    printf("Ceci est le serveur\n") ;
    if ( argc!=7)
    {
        printf("ser ser port cli\n") ;
        exit(1) ;
    }

    /* Récupération IP & port   */
    IpSocket= inet_addr(argv[1]);
    PortSocket = atoi(argv[2]);
    Desc1=creer_socket(SOCK_DGRAM,&IpSocket,PortSocket,&sthis1);

    if ( Desc1 == -1 )
        die("CreateSockets:") ;
    else
        fprintf(stderr,"CreateSockets %d\n",Desc1) ;

    IpSocket= inet_addr(argv[3]);
    PortSocket = atoi(argv[4]);
    Desc2=creer_socket(SOCK_DGRAM,&IpSocket,PortSocket,&sthis1);

    if ( Desc2 == -1 )
        die("CreateSockets:") ;
    else
        fprintf(stderr,"CreateSockets %d\n",Desc1) ;

    IpSocket= inet_addr(argv[5]);
    PortSocket = atoi(argv[6]);
    Desc3=creer_socket(SOCK_DGRAM,&IpSocket,PortSocket,&sthis1);

    if ( Desc3 == -1 )
        die("CreateSockets:") ;
    else
        fprintf(stderr,"CreateSockets %d\n",Desc1) ;



    while(1) {

        FD_ZERO(&readfs) ;
        FD_SET(0, &readfs); /* on teste le clavier */
        FD_SET(Desc1, &readfs);
        FD_SET(Desc2, &readfs);
        FD_SET(Desc3, &readfs);

        tv.tv_sec = 30 ; /* 30 secondes de timeout */
        tv.tv_usec = 100000 ; /* temps en micro secondes */

        if((rc = select( FD_SETSIZE, &readfs, NULL, NULL, &tv)) < 0)
            die("select()");
        else {
            if (rc == 0)
                printf("Timeout !!!!!!! \n");
            else {
                if (FD_ISSET(0, &readfs)) {
                    car = ReadChar(); /* fonction bibliothèque */
                    printf("La touche pressee est %c \n", car);
                    if ((car == 'q') || (car == 'Q'))
                        exit(0);
                }
                if (FD_ISSET(Desc1, &readfs)) {
                    Traitementrequete(Desc1);
                }
                if (FD_ISSET(Desc2, &readfs)) {
                    Traitementrequete(Desc2);
                }
                if (FD_ISSET(Desc3, &readfs)) {
                    Traitementrequete(Desc3);
                }

            }
        }

    }

}
