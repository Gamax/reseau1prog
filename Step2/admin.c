/******************************************
  Herman Vanstapel
  2017 Basé sur Fichiers
*******************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "data.h"
#include "lib2018DD.h"

void DelNewLine(char *Chaine)
{
    Chaine[strlen(Chaine)-1] = 0 ;
}

char ReadChar()
{
    char Tampon[80] ;
    fgets(Tampon,sizeof Tampon,stdin ) ;
    return Tampon[0] ;
}

void MonPrintf(char* tempo, int espace,int taille )
{
    int Count ;
    printf("%s",tempo) ;
    Count = espace-taille ;
    while ( Count>0 )
    {
        printf(" ");
        Count-- ;
    }
}

void AfficheEnteteArticleDD()
{
    char Tampon[80] ;
    sprintf(Tampon,"%s","Ref" ) ;MonPrintf(Tampon,4,strlen(Tampon)) ;
    sprintf(Tampon,"%s","Descrition") ;    MonPrintf(Tampon,50,strlen(Tampon)) ;
    sprintf(Tampon,"%s","Qt") ; MonPrintf(Tampon,4,strlen(Tampon)) ;
    sprintf(Tampon,"%s","Dim") ; MonPrintf(Tampon,4,strlen(Tampon)) ;
    printf("\n") ;
}

void AfficheArticleDD(struct ArticleDD   *UnRecord)
{
    char Tampon[80] ;
    sprintf(Tampon,"%d",UnRecord->Reference ) ;  MonPrintf(Tampon,4,strlen(Tampon)) ;
    sprintf(Tampon,"%s",UnRecord->Description) ;    MonPrintf(Tampon,50,strlen(Tampon)) ;
    sprintf(Tampon,"%d",UnRecord->Quantite) ; MonPrintf(Tampon,4,strlen(Tampon)) ;
    sprintf(Tampon,"%d",UnRecord->DimensionEcran) ; MonPrintf(Tampon,4,strlen(Tampon)) ;
    printf("\n") ;
}

void SaiSieArticleDD(int Reference, struct ArticleDD *UnRecord )
{
    char Tampon[80] ;

    printf("Reference :%d \n",Reference) ;
    UnRecord->Reference = Reference ;
    printf("Saisie Description :") ;
    fgets(UnRecord->Description,sizeof UnRecord->Description,stdin ) ;
    DelNewLine(UnRecord->Description) ;
    printf("Saisie Dimension :") ;
    fgets(Tampon,sizeof Tampon,stdin ) ;
    UnRecord->DimensionEcran = atoi(Tampon);
    printf("Saisie Places :") ;
    fgets(Tampon,sizeof Tampon,stdin ) ;
    UnRecord -> Quantite = atoi(Tampon) ;

    AfficheEnteteArticleDD() ;
    AfficheArticleDD(UnRecord) ;
    printf("-----------------------\n") ;
    return ;
}

int NombreArticlesDD(char *NomFichier)
{
    FILE *sortie ;
    sortie = fopen(NomFichier,"r") ;
    if ( sortie == NULL )
    {
        fprintf(stderr,"Echec Ouverture\n") ;
        return 0 ;
    }
    else
        fprintf(stderr,"Ouverture reussie \n") ;
    fseek(sortie, 0, SEEK_END ) ;
    return (ftell(sortie)/ sizeof(struct ArticleDD )) ;
}

void CreationAjoutArticleDD(char *NomFichier,struct ArticleDD *UnRecord )
{
    FILE *sortie ;
    char Redo ;
    int nbr ;

    sortie = fopen(NomFichier,"a") ; /* Si le fichier existe, on le cree sinon on ajoute */
    if ( sortie == NULL )
    {
        fprintf(stderr,"Echec Ouverture\n") ;
        exit(0) ;
    }
    else
        fprintf(stderr,"Ouverture reussie \n") ;


    printf("Position actuelle dans le fichier %ld\n",ftell(sortie)) ;
    nbr = fwrite(UnRecord,sizeof(struct ArticleDD ),1,sortie) ;
    fflush(sortie) ;
    fclose(sortie) ;
}



void AfficheFacture(struct FactureDD *UneFacture)
{
    char Tampon[80] ;
    sprintf(Tampon,"%d",UneFacture->NumeroFacturation ) ; MonPrintf(Tampon,4,strlen(Tampon)) ;
    sprintf(Tampon,"%s",UneFacture->NomClient) ; MonPrintf(Tampon,60,strlen(Tampon)) ;
    sprintf(Tampon,"%d",UneFacture->Quantite ) ; MonPrintf(Tampon,4,strlen(Tampon)) ;
    sprintf(Tampon,"%d",UneFacture->Reference) ; MonPrintf(Tampon,4,strlen(Tampon)) ;
    sprintf(Tampon,"%d",UneFacture->DateFacturation ) ; MonPrintf(Tampon,8,strlen(Tampon)) ;

    printf("\n") ;
}

void ListingArticlesDD(char *NomFichier)
{
    struct ArticleDD  UnRecord ;
    FILE *sortie ;
    char Tampon[80] ;
    int  Numero ;
    int  nbr ;

    sortie = fopen(NomFichier,"r") ; /* Si le fichier existe, on le cree sinon on ajoute */
    if ( sortie == NULL )
    {
        fprintf(stderr,"Echec Ouverture\n") ;
        exit(0) ;
    }
    else
        fprintf(stderr,"Ouverture reussie \n") ;

    AfficheEnteteArticleDD() ;
    nbr = fread(&UnRecord,sizeof(UnRecord),1,sortie) ;

    while ( !feof(sortie) )
    {
        fprintf(stderr,"Record lu %d et Position actuelle dans le fichier %ld\n",nbr,ftell(sortie)) ;
        AfficheArticleDD( &UnRecord) ;
        nbr = fread(&UnRecord,sizeof(UnRecord),1,sortie) ;
    }
    fclose(sortie) ;
}

void ListingFacturesDD(char *NomFichier)
{
    struct FactureDD UneFacture ;
    FILE *sortie ;
    char Tampon[80] ;
    int  Numero ;
    int  nbr ;

    sortie = fopen(NomFichier,"r") ; /* Si le fichier existe, on le cree sinon on ajoute */
    if ( sortie == NULL )
    {
        fprintf(stderr,"Echec Ouverture\n") ;
        exit(0) ;
    }
    else
        fprintf(stderr,"Ouverture reussie \n") ;


    nbr = fread(&UneFacture,sizeof(struct FactureDD),1,sortie) ;

    while ( !feof(sortie) )
    {
        fprintf(stderr,"Record lu %d et Position actuelle dans le fichier %ld\n",nbr,ftell(sortie)) ;
        AfficheFacture( &UneFacture) ;
        nbr = fread(&UneFacture,sizeof(struct FactureDD ),1,sortie) ;
    }
    fclose(sortie) ;
}


main()
{
    char Choix,temp ;
    char Tampon[80] ;
    int res , ref, ret;
    struct ArticleDD UnRecord ;
    int Numero ;
    while(1)
    {
        printf("----------------------\n") ;
        printf("1) Ajout              \n") ;
        printf("2) Articles              \n") ;
        printf("3) Recherche          \n") ;
        printf("4) Achat              \n") ;
        printf("5) Factures           \n") ;
        printf("6) A propos           \n") ;
        printf("7) exit               \n") ;
        printf("----------------------\n") ;
        Choix=ReadChar() ;
        switch(Choix)
        {
            case '1':
            {
                struct ArticleDD  UnRecord ;
                FILE *sortie ;
                char Redo ;

                Redo='y' ;
                while ( Redo=='Y' || Redo=='y')
                {
                    int Nombre ;
                    Nombre= NombreArticlesDD("ArticlesDD") ;
                    SaiSieArticleDD(Nombre+1, &UnRecord ) ;
                    CreationAjoutArticleDD("ArticlesDD",&UnRecord) ;
                    printf("Encoder un autre (Y/N) ?)") ;
                    Redo=ReadChar() ;
                }

                break ;
            }
            case '2': ListingArticlesDD("ArticlesDD") ;
                break ;
            case '3':
                printf("Entrez la référence de l'article désiré :");
                temp = ReadChar();
                ref = atoi(&temp);
                ret = RechercheDD("ArticlesDD",ref,&UnRecord);

                if(ret == -1) {
                    printf("Problème lors de l'ouverture du fichier\n");
                    break;
                }

                if(ret == 0){
                    printf("Référence non trouvée\n");
                    break;
                }

                printf("Article trouvé !\n");
                AfficheEnteteArticleDD();
                AfficheArticleDD(&UnRecord);
                break ;
            case '5': ListingFacturesDD("FactureDD") ;
                break ;
            case '6': AProposDD("V 1","Herman","Vanstapel") ;
                break ;


            case '7': exit(0) ;
        }
    }
    return 0;
}
