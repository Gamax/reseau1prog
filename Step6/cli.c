/*---------------------------------------------------------------
   Vanstapel Herman
   ex02\cli.c

 Le client dit bonjour en utilisant un structure  et
 le serveur fait de même
------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include "../udplib/udplib.h"
#include "structure.h"

int NbrAlarmes=0 ;

static void signal_handler(int sig)
{

}

void die(char *s)
{
    perror(s);
    exit(1);
}

char ReadChar()
{
    char Tampon[80] ;
    fgets(Tampon,sizeof Tampon,stdin ) ;
    return Tampon[0] ;
}

void DelNewLine(char *Chaine)
{
    Chaine[strlen(Chaine)-1] = 0 ;
}

int main(int argc, char *argv[])
{
    int rc ;
    int ret;
    int Desc ;
    int tm ;
    unsigned int time_interval ;

    u_long  IpSocket , IpServer;
    u_short PortSocket, PortServer ;

    struct sockaddr_in sthis ; /* this ce programme */
    struct sockaddr_in sos ; /* s = serveur */
    struct sockaddr_in sor ; /* r = remote */
    struct RequeteDD UneRequete ;

    memset(&sthis,0,sizeof(struct sockaddr_in)) ;
    memset(&sos,0,sizeof(struct sockaddr_in)) ;
    memset(&sor,0,sizeof(struct sockaddr_in)) ;

    if (argc!=5)

    {
        printf("cli client portc serveur ports\n") ;
        exit(1) ;
    }

    /* Récupération IP & port   */
    IpSocket= inet_addr(argv[1]);
    PortSocket = atoi(argv[2]);

    IpServer = inet_addr(argv[3]) ;
    PortServer = atoi(argv[4]);

    // Desc = CreateSockets(&psoo,&psos,,atoi(argv[2]),argv[3],atoi(argv[4])) ;
    Desc=creer_socket(SOCK_DGRAM,&IpSocket,PortSocket,&sthis);

    if ( Desc == -1 )
        die("CreateSockets:") ;
    else
        fprintf(stderr,"CreateSockets %d\n",Desc) ;

    sos.sin_family = AF_INET ;
    sos.sin_addr.s_addr= IpServer ;
    sos.sin_port = htons(PortServer) ;

    int num,choix=0,i1,i2,i3,Compteur = 0;
    char Tampon[80], temp[10], temp1;

    struct sigaction act;
    act.sa_handler = signal_handler;
    act.sa_flags = 0 ;
    sigemptyset (&act.sa_mask);
    rc = sigaction (SIGALRM, &act, NULL);

    while(choix >= 0 ) {

        printf("-----------------\n1) Demander une référence\n2) Passer une commande\n3) Quitter\n----------------\nChoix :");
        choix = ReadChar();

        switch(choix){
            case '1':
                printf("Référence :");
                fgets(temp,sizeof(temp),stdin);
                num = atoi(temp);



                redo1:

                UneRequete.Type = Question;
                UneRequete.Reference = num;
                UneRequete.Numero = Compteur;
                time_interval = 9 ;
                ret = alarm(time_interval);
                rc = SendDatagram(Desc, &UneRequete, sizeof(struct RequeteDD), &sos);

                if (rc == -1)
                    die("SendDatagram");
                else
                    fprintf(stderr, "Envoi de %d bytes\n", rc);

                while(1) {
                    memset(&UneRequete, 0, sizeof(struct RequeteDD));
                    tm = sizeof(struct RequeteDD);
                    rc = ReceiveDatagram(Desc, &UneRequete, tm, &sor);

                    if ( rc<=0)
                    {
                        perror("error sur receive:") ;
                        fprintf(stderr,"rc %d errno:%d \n",rc,errno) ;
                        goto redo1 ;
                    }

                    if ( UneRequete.Numero != Compteur )
                        printf("Doublon %d !!!!!\n",UneRequete.Numero) ;
                    else
                        break ;
                }
                ret = alarm(0);

                if(UneRequete.Type == Fail){
                    printf("Article non trouvé\n");
                    break;
                }

                fprintf(stderr, "bytes recus:%d:%s Qt : %d Dim : %d\n", rc, UneRequete.Description, UneRequete.Quantite, UneRequete.DimensionEcran);
                break;
            case '2':
                printf("NomClient :");
                fgets(Tampon,40,stdin);
                DelNewLine(Tampon);
                printf("Entrez la référence de l'article désiré :");
                fgets(temp,10,stdin);
                i1 = atoi(temp);
                printf("Entrez la quantité de l'article désirée :");
                fgets(temp,10,stdin);
                i2 = atoi(temp);
                printf("Entrez la date de la facture :");
                fgets(temp,10,stdin);
                i3 = atoi(temp);

                redo2:

                UneRequete.Type = Achat;
                UneRequete.Reference = i1;
                UneRequete.Quantite = i2;
                UneRequete.Date = i3;
                UneRequete.Numero = Compteur;
                strncpy(UneRequete.NomClient,Tampon,40* sizeof(char));


                time_interval = 9 ;
                ret = alarm(time_interval);
                rc = SendDatagram(Desc,&UneRequete,sizeof(struct RequeteDD),&sos);

                if (rc == -1)
                    die("SendDatagram");
                else
                    fprintf(stderr, "Envoi de %d bytes\n", rc);

                while(1) {
                    memset(&UneRequete, 0, sizeof(struct RequeteDD));
                    tm = sizeof(struct RequeteDD);
                    rc = ReceiveDatagram(Desc, &UneRequete, tm, &sor);

                    if ( rc<=0)
                    {
                        perror("error sur receive:") ;
                        fprintf(stderr,"rc %d errno:%d \n",rc,errno) ;
                        goto redo2 ;
                    }

                    if ( UneRequete.Numero != Compteur )
                        printf("Doublon %d !!!!!\n",UneRequete.Numero) ;
                    else
                        break ;
                }
                ret = alarm(0);

                if(UneRequete.Type == Fail){
                    printf("Achat Non Réussi\n");
                }else{
                    fprintf(stderr, "bytes recus:%d\nAchat réussi Facture : %d\n", rc, UneRequete.NumeroFacture);
                }
                break;
            default:
                choix = -1;
                break;
        }
    Compteur++;
    }

    close(Desc) ;
}
