
enum TypeRequete {
    Question = 1,
    Achat = 2,
    Livraison = 3,
    OK = 4,
    Fail = 5,
    Reponse = 6,
};

struct RequeteDD
{
    enum TypeRequete Type ;
    int Numero ; // Contient le numéro de la requete
    int NumeroFacture ;
    int Date ;
    int Reference ;
    int Quantite ;
    int Prix ;
    char Description[40] ;
    int DimensionEcran;
    char NomClient[80] ;
    unsigned short crc;
} ; 