#include "lib2018DD.h"

void AProposDD(char *Version,char *Nom1,char* Nom2)
{
    printf("Version : %s \n",Version ) ;
    printf("Nom1 : %s \n",Nom1 ) ;
    printf("Nom2 : %s \n",Nom2 ) ;
}

int RechercheDD(char* NomFichier,int Reference ,struct ArticleDD
*UnRecord){

    FILE* f = fopen(NomFichier,"r");

    if(!f)
        return -1; //fail ouverture fichier

    while(!feof(f)) {
        fread(UnRecord, sizeof(struct ArticleDD), 1, f);
        if(UnRecord->Reference == Reference){
            fclose(f);
            return Reference;
        }

    }

    fclose(f);
    return 0;

}

int ReservationDD(char* NomFichier,int Reference ,int Quantite ){
    FILE* f = fopen(NomFichier,"r+");

    if(!f)
        return -1; //fail ouverture fichier

    struct ArticleDD UnRecord;

    while(!feof(f)) {
        fread(&UnRecord, sizeof(struct ArticleDD), 1, f);
        if(UnRecord.Reference == Reference && UnRecord.Quantite >= Quantite){
            UnRecord.Quantite -= Quantite;
            fseek(f,-sizeof(struct ArticleDD),SEEK_CUR);
            fwrite(&UnRecord,sizeof(struct ArticleDD),1,f);
            fclose(f);
            return 1;
        }

    }

    fclose(f);
    return 0;
}

int FacturationDD(char NomFichier[80], char NomClient[40], int Date,int Quantite,int Reference){
    FILE* f = fopen(NomFichier,"a");
    if(!f)
        return -1; //fail fichier

    struct FactureDD facture;
    facture.Reference = Reference;
    facture.DateFacturation = Date;
    facture.Quantite = Quantite;
    facture.NumeroFacturation = ftell(f) / sizeof( struct FactureDD ) + 1 ;
    strncpy(facture.NomClient,NomClient,40*sizeof(char));
    fwrite(&facture, sizeof(struct FactureDD),1,f);
    fclose(f);
    return facture.NumeroFacturation;


}


