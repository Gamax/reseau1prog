/*--------------------------------------
  Herman Vanstapel
  
  ex02\ser.c 
  
  Un serveur recevant une structure 
----------------------------------------*/

#include <stdio.h>
#include <string.h>
#include "../udplib/udplib.h"
#include "structure.h"
#include "lib2018DD.h"
#include "data.h"

void die(char *s)
{
    perror(s);
    exit(1);
}

int main(int argc,char *argv[])
{
    int rc ;
    int Desc ;
    struct sockaddr_in sthis ; /* this ce programme */
    struct sockaddr_in sos ; /* s = serveur */
    struct sockaddr_in sor ; /* r = remote */

    u_long  IpSocket ;
    u_short PortSocket ;

    int tm ;
    struct RequeteDD UneRequete, UneRequete2 ;

    memset(&sthis,0,sizeof(struct sockaddr_in)) ;
    memset(&sos,0,sizeof(struct sockaddr_in)) ;
    memset(&sor,0,sizeof(struct sockaddr_in)) ;

    printf("Ceci est le serveur\n") ;
    if ( argc!=3)
    {
        printf("ser ser port cli\n") ;
        exit(1) ;
    }

    /* Récupération IP & port   */
    IpSocket= inet_addr(argv[1]);
    PortSocket = atoi(argv[2]);
    // Desc = CreateSockets(&psoo,&psos,,atoi(argv[2]),argv[3],atoi(argv[4])) ;
    Desc=creer_socket(SOCK_DGRAM,&IpSocket,PortSocket,&sthis);

    if ( Desc == -1 )
        die("CreateSockets:") ;
    else
        fprintf(stderr,"CreateSockets %d\n",Desc) ;

    int res;

    struct ArticleDD UnRecord;

    while(1) {

        tm = sizeof(struct RequeteDD);
        rc = ReceiveDatagram(Desc, &UneRequete, tm, &sor);
        if (rc == -1)
            die("ReceiveDatagram");
        else
            fprintf(stderr, "bytes recus:%d:%s\n", rc, UneRequete.Description);

        printf("Received packet from %s:%d\n", inet_ntoa(sor.sin_addr), ntohs(sor.sin_port));


        switch (UneRequete.Type) {
            case Question:
                fprintf(stderr, "A rechercher %d \n", UneRequete.Reference);

                res = RechercheDD("ArticlesDD", UneRequete.Reference, &UnRecord);

                //fprintf(stderr,"res :%d Reference:%s Quantité %d\n",res,UnRecord.Description,UnRecord.Quantite ) ;

/* reponse avec psor qui contient toujours l'adresse du dernier client */

                strncpy(UneRequete2.Description, UnRecord.Description, sizeof(UneRequete2.Description));

                UneRequete2.Quantite = UnRecord.Quantite;
                UneRequete2.Numero = UneRequete.Numero;
                UneRequete2.Reference = UneRequete.Reference;
                UneRequete2.DimensionEcran = UnRecord.DimensionEcran;

                if (res)
                    UneRequete2.Type = OK;
                else
                    UneRequete2.Type = Fail;

                rc = SendDatagram(Desc, &UneRequete2, sizeof(struct RequeteDD), &sor);

                if (rc == -1)
                    perror("SendDatagram:");
                else
                    fprintf(stderr, "bytes:%d\n", rc);
                break;
            default:
                break;
        }
    }

    close(Desc) ;
}
