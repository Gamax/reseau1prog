#ifndef LIB2018DD_H
#define LIB2018DD_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "data.h"


void AProposDD(char *Version,char *Nom1,char* Nom2) ;

int RechercheDD(char* NomFichier,int Reference ,struct ArticleDD
*UnRecord) ;

int ReservationDD(char* NomFichier,int Reference ,int Quantite );

int FacturationDD(char NomFichier[80], char NomClient[40], int Date,int Quantite,int Reference);

int verifFactureDD(char NomFichier[80], int date, char NomClient[80]);

#endif LIB2018DD_H
